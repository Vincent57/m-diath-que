#include "mediatheque.h"

Book initBook(){
    Book book=(Book)malloc(sizeof(struct Book));
    book->Date=0;
    book->nameAuthor=NULL;
    book->nameBook=NULL;

    return book;
}

DVD initDVD(){
    DVD dvd=(DVD)malloc(sizeof(struct DVD));
    dvd=NULL;

    return dvd;
}

CD initCD(){
    CD cd=(CD)malloc(sizeof(struct CD));
    cd=NULL;

    return cd;
}

Bibliotheque initBibliotheque(){
    Bibliotheque b=(Bibliotheque)malloc(sizeof(struct Bibliotheque));

    b->n=0;
    b->tete=(Document)malloc(sizeof(struct Document));
    b->queue=(Document)malloc(sizeof(struct Document));
    b->tete->precedent=b->tete;
    b->queue->suivant=b->queue;
    b->tete->suivant=b->queue->precedent;
    b->queue->precedent=b->tete->suivant;

    return b;
}

Document initDocument(){
    Document doc=(Document)malloc(sizeof(struct Document));

    doc->val=NULL;
    doc->type=NULL;
    doc->precedent=NULL;
    doc->suivant=NULL;
}

void print_book(Book book){
    printf("Nom du livre : %s\n", book->nameBook);
    printf("Nom de l'auteur.e : %s\n", book->nameAuthor);
    printf("Date de parution : %i\n\n", book->Date);
}

void print_dvd(DVD dvd){
    printf("Nom du film : %s\n", dvd->nameDVD);
    printf("Nom du real : %s\n", dvd->nameReal);
    printf("Duree du film :%i\n\n", dvd->time);
}

void print_cd(CD cd){
    printf("Nom du cd : %s\n", cd->nameCD);
    printf("Nom du chanteur : %s\n", cd->nameSinger);
    printf("Durée du cd: %i\n\n", cd->time);
}

void print_doc(Bibliotheque b, void *doc){
    Document d=(Document)doc;

    if (d->type=="livre")
        print_book((Book)d);

    if (d->type=="dvd")
        print_dvd((DVD)d);

    if (d->type=="cd")
        print_cd((CD)d);
}

void Del_biblio(Bibliotheque b){
    free(b);
}

void list_doc(Bibliotheque b){
    char c;

    if (b->n==0){
        printf("Il n'y a aucun document d'enregistré\n");
        printf("Appuyer sur Enter pour revenir au menu principal\n");
        while (c!='\n')
            c=getchar();
    }else{
        string type=(string)malloc(MAX);
        Document d=b->tete->suivant;
        int i=0;

        printf("Quel type de documents voulez vous voir : ");
        read(type);
    
        while (i<b->n)
        {
            if (d->type==type || type=="tout" || type=="Tout"){
                printf("%i: ", i+1);
                print_doc(b, d);
                i++;
            }
            d=d->suivant;
        }
        free(type);
    }
}

int check_ortho(string type, string entry){
    FILE* f=fopen(entry, "r");
    string lecture=(string)malloc(MAX);

    if (f!=NULL){
        printf("ok");
        while (fgets(lecture, MAX, f)!=NULL){
            printf("%s", lecture);
            if (lecture==entry)
                return 1;
        }

        fclose(f);
        return 0;
    }

    return 0;
}

void voidBuffer()
{
    int c = 0;
    while (c != '\n' && c != EOF)
        c = getchar();
}

void read(string chain){
    string pos=NULL;
    if (fgets(chain, MAX, stdin) != NULL){
        pos = strchr(chain, '\n');
        if (pos != NULL)
            *pos = '\0';
    }
}

void Add_doc(Bibliotheque b, void *doc, string type){
    Document cel=b->tete;

    for (int i=0; i<b->n; i++)
        cel=cel->suivant;

    cel->val=doc;
    cel->type=type;
    cel->precedent->suivant=cel;
    cel->suivant->precedent=cel;

    b->n++;
}

void Add_book(Bibliotheque b){
    Book book=initBook();
    string nameBook = (string)malloc(MAX);
    string nameAuthor = (string)malloc(MAX);
    int date=0;

    printf("\nNom du livre : ");
    read(nameBook);
    
    printf("Nom de l'auteur.e : ");
    read(nameAuthor);

    printf("Date de parution :");
    scanf("%i", &date);

    book->nameBook=nameBook;
    book->nameAuthor=nameAuthor;
    book->Date=date;

    Add_doc(b, book, "Book");

    free(nameAuthor);
    free(nameBook);
    free(book);
}

void Add_dvd(Bibliotheque b){
    DVD dvd=initDVD();
    string nameDvd=(string)malloc(MAX);
    string nameReal=(string)malloc(MAX);
    int time=0;
    
    printf("\nNom du DVD : ");
    read(nameDvd);

    printf("Nom du réalisateur : ");
    read(nameReal);

    printf("Durée du film :");
    scanf("%i", &time);

    dvd->nameDVD=nameDvd;
    dvd->nameReal=nameReal;
    dvd->time=time;

    Add_doc(b, dvd, "DVD");

    free(nameReal);
    free(nameDvd);
    free(dvd);
}

void Add_cd(Bibliotheque b){
    CD cd=initCD();
    string nameCD=(string)malloc(MAX);
    string nameSinger=(string)malloc(MAX);
    int time=0;
    
    printf("\nNom du CD : ");
    read(nameCD);

    printf("Nom du chateur : ");
    read(nameSinger);

    printf("Durée du CD :");
    scanf("%i", &time);

    cd->nameCD=nameCD;
    cd->nameSinger=nameSinger;
    cd->time=time;

    Add_doc(b, cd, "CD");

    free(nameSinger);
    free(nameCD);
    free(cd);
}

void Add_menu(Bibliotheque b){
    string typeDoc = (string)malloc(MAX);

    do{
        //system("clear");
        printf("Quel type de documents voulez vous ajouter ? (Livre,DVD,CD)\n");
        voidBuffer();
        read(typeDoc);

        if (check_ortho("book", typeDoc)==1){
            Add_book(b);
        }

        if (check_ortho("dvd", typeDoc)==1){
            Add_dvd(b);
        }

        if (check_ortho("cd", typeDoc)==1){
            Add_cd(b);
        }else{
            printf("Veuillez entrer un nom valide\n");
        }
    }while (strcmp(typeDoc, "Quitter") != 0);

    free(typeDoc);
}

void main(){
    
    int choix=0;
    Bibliotheque b=initBibliotheque();

    do{
        system("clear");
        printf("#=========================================#\n");
        printf("\tBienvenue à la médiathèque !\n");
        printf("#=========================================#\n\n");
        printf("Séléctionner votre choix :\n");
        printf("1: Ajouter un document\n");
        printf("2: Supprimer un document\n");
        printf("3: Afficher un document\n");
        printf("4: Quitter\n\n");
        printf("Votre choix : ");
        scanf("%i", &choix);

        switch (choix){
            case 1:
                Add_menu(b);
                break;
            case 2:
                //supprimerDoc(b);
                break;

            case 3:
                list_doc(b);
                break;

            case 4:
                break;

            default:
                printf("Entrez un nombre valide\n\n");
                break;
        }
    }while(choix!=4);

    free(b);
}