#ifndef MEDIA_H
#define MEDIA_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX 100

typedef struct Bibliotheque* Bibliotheque;
typedef struct Document* Document;
typedef struct Book* Book;
typedef struct DVD* DVD;
typedef struct CD* CD;
typedef char* string;

struct Document{
    void *val;
    string type;
    Document suivant;
    Document precedent;
};

/*Structure Book*/
struct Book{
    string nameBook;
    string nameAuthor;
    int Date;
};

/*Structure DVD*/
struct DVD{
    string nameDVD;
    string nameReal;
    int time;
};

/*Structure CD*/
struct CD{
    string nameCD;
    string nameSinger;
    int time;
};

/*Structure Bibliothèque*/
struct Bibliotheque{
    Document tete;
    Document queue;
    int n;
};

/*----------------------------*/
/*Fonctions de la bibliotheque*/
/*----------------------------*/

Bibliotheque initBibliotheque();
void Add_biblio(Bibliotheque b, void *doc);
void Del_biblio(Bibliotheque b);
void print_biblio(Bibliotheque b);

/*----------------------*/
/*Fonctions de documents*/
/*----------------------*/

Document initDocument();
void Add_doc(Bibliotheque b, void *doc, string type);
void print_doc(Bibliotheque b, void *doc);
void Del_doc(Bibliotheque b, void *doc);
void list_doc(Bibliotheque b);

/*-------------------*/
/*Fonctions de Books*/
/*-------------------*/

Book initBook();
void Add_book(Bibliotheque b);
void print_book(Book book);

/*----------------*/
/*Fonctions de DVD*/
/*----------------*/

DVD initDVD();
void Add_dvd(Bibliotheque b);
void print_dvd(DVD dvd);

/*---------------*/
/*Fonctions de CD*/
/*---------------*/

CD initCD();
void Add_cd(Bibliotheque b);
void print_cd(CD cd);

/*------*/
/*Autres*/
/*------*/

void read(string chaine);
void voidBuffer();

int check_ortho(string type, string entry);

#endif